// Tugas No 1 Looping dengan While 
console.log("No. 1 Looping dengan While")
var flag = 2
console.log("LOOPING PERTAMA")
while (flag <= 20) {
    console.log(flag + " - I love coding")
    flag += 2
}
console.log("LOOPING KEDUA")
while (flag > 2) {
    flag -= 2
    console.log(flag + " - I will become a mobile developer")
}
console.log("")

// Tugas No 2 Looping dengan For
console.log("No. 2 Looping dengan For")
for (var i = 1; i <= 20; i++) {
    if (i % 2 == 1) {
        console.log(i + " - Santai")
    }
    else if (i % 3 === 0 && i % 2 !== 0) {
        console.log(i + " - I Love Coding")
    } 
    else {
        console.log(i + " - Berkualitas")
    }
}
console.log("")

// Tugas No 3 Membuat Persegi Panjang dengan Looping
console.log("No. 3 Membuat Persegi Panjang Dengan Looping")
for (var i = 0; i < 4; i++) {
    var crsh = ""
    for (var j = 0; j < 8; j++) {
        crsh += "#"
    }
    console.log(crsh)
}
console.log("")

// Tugas No 4 Membuat Tangga dengan Looping
console.log("No. 4 Membuat Tangga Dengan Looping")
for (var i = 0; i < 7; i++) {
    var crsh = ""
    for (var j = 0; j < (i + 1); j++) {
        crsh += "#"
    }
    console.log(crsh)
}
console.log("")


// Tugas No 5 Membuat Papan Catur  dengan Looping
console.log("No. 5 Membuat Papan Catur  dengan Looping")
for (var col = 0; col < 8; col++) {
    var crsh = ""
    for (var row = 0; row < 8; row++) {
        if (col % 2 === 0) {
            if (row % 2 === 0) {
                crsh += " "
            }
            else {
                crsh += "#"
            }
        }
        else {
            if (row % 2 === 0) {
                crsh += "#"
            }
            else {
                crsh += " "
            }
        }
    }
    console.log(crsh)
}
