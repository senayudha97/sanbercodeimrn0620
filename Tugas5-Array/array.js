// Tugas no 1
console.log('Tugas no 1 Range');
function range(startNum, finishNum ){
    var arr;
    if(startNum == null || finishNum == null){
        arr = -1;
    }
    else{
        arr = [startNum];
        startNum++;
        while (startNum <= finishNum) {
            arr.push(startNum);
            startNum++;
        }
    }

    return arr;
}

console.log(range(1, 7));
console.log('');

//Tugas No 2
console.log('Tugas no 2 Range with Step');
function rangeWithStep(startNum, finishNum, step = 1){
    var arr;
    if(startNum == null || finishNum == null){
        arr = -1;
    }
    else{
        arr = [startNum];
        startNum += step;
        while (startNum <= finishNum) {
            arr.push(startNum);
            startNum += step;
        }
    }

    return arr;
}

console.log(rangeWithStep(5,50,2));
console.log('');

//Tugas No 3
console.log('Tugas No 3 Sum of Range');
function sum(startNum, finishNum, step = 1){
    var arr, total = 0;
    if(startNum == null && finishNum == null){
        total = 0;
    }
    else if (startNum == null || finishNum == null){
        total = 1;
    }
    else{
        if (startNum > finishNum) {
            var temp  = startNum;
            startNum = finishNum;
            finishNum = temp;  
        }
        arr = [startNum];
        startNum += step;
        while (startNum <= finishNum) {
            arr.push(startNum);
            startNum += step;
        }
        var i = 0;
        while (i <= arr.length-1) {
            total += arr[i];
            i+=1;
        }
    }

    return total;
}

console.log(sum(20, 10, 2));
console.log('');

//Tugas No 4
console.log('Tugas No 4 Array MultiDimensi');
function dataHandling(param)
{
    for (let i = 0; i <= param.length-1; i++) {
        console.log("Nomor ID:"+param[i][0]);
        console.log("Nama Lengkap:"+param[i][1]);
        console.log("TTL:"+param[i][2]+" "+param[i][3]);
        console.log("Hobby:"+param[i][4]);        
        console.log("");

    }

}
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
];
dataHandling(input);
console.log("");

//Tugas No 5
console.log("Tugas No 5 Balik Kata");
function balikKata(param) {
    var kata = param, hasil="";

    for (let i = kata.length-1; i >= 0; i--) {
        hasil += kata[i];
    }
    
    console.log(hasil);
}

balikKata("Nofia Hidayati");
console.log('');

//Tugas No 6
console.log('Tugas No 6 Metode Array');
function dataHandling2(param)   
{
    param.splice(4, 4, "Pria", "SMA Internasional Metro");
    param.splice(1, 1, "Roman Alamsyah Elsharawy");
    
    console.log(param);

    var get_date = param[3];
    month = get_date.split("/");
        
    switch(String(month[1])){
        case '01': {bulan = "Januari"; break;}
        case '02': {bulan = "Feburari"; break;}
        case '03': {bulan = "Maret"; break;}
        case '04': {bulan = "April"; break;}
        case '05': {bulan = "Mei"; break;}
        case '06': {bulan = "Juni"; break;}
        case '07': {bulan = "Juli"; break;}
        case '08': {bulan = "Agustus"; break;}
        case '09': {bulan = "September"; break;}
        case '10': {bulan = "Oktober"; break;}
        case '11': {bulan = "November"; break;}
        case '12': {bulan = "Desember"; break;}
    }

    console.log(bulan);
    console.log(month);

    new_month = month.join("-");
    console.log(new_month);

    get_name = param[1];
    new_name = get_name.slice(0, 15);
    console.log(new_name);



}
var input = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input);
console.log("");
