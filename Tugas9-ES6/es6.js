//NO 1
console.log('Tuga No 1')
const golden =() =>{
  console.log("this is golden!!")
}
 
golden()
console.log('')


//NO 2
console.log('Tuga No 2')
const newFunction = function literal(firstName, lastName){
  return {
    firstName,
    lastName,
    fullName: function(){
      console.log(firstName + " " + lastName)
      return 
    }
  }
}
 
//Driver Code 
newFunction("William", "Imoh").fullName() 

//NO 3
console.log('Tuga No 3')
const newObject = {
  firstName: "Harry",
  lastName: "Potter Holt",
  destination: "Hogwarts React Conf",
  occupation: "Deve-wizard Avocado",
  spell: "Vimulus Renderus!!!"
}


const { firstName, lastName, destination, occupation, spell } = newObject;

console.log(firstName, lastName, destination, occupation)
console.log('')


//NO 4
console.log('Tuga No 4')
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
let combined = [...west, ...east]
//Driver Code
console.log(combined)
console.log('')

//NO 5
console.log('Tuga No 5')
const planet = "earth"
const view = "glass"

//ES5
var before = 'Lorem ' + view + 'dolor sit amet, ' +  
    'consectetur adipiscing elit,' + planet + 'do eiusmod tempor ' +
    'incididunt ut labore et dolore magna aliqua. Ut enim' +
    ' ad minim veniam'
//ES6
var after = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, consectetur adipiscing elit, ${planet} do eiusmod tempor incididunt 
ut labore et dolore magna aliqua. Ut enim ad minim veniam`
 
// Driver Code
console.log(after) 