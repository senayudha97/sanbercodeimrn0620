//Tugas No 1
console.log("Tugas No 1 Membuat Funtion Mengembalikan Nilai 'Halo Sanbers!'");
function teriak() {
    return "Halo Sanbers!";
}

console.log(teriak());
console.log("");

//Tugas No 2
console.log("Tugas No 2 Membuat Funtion Mengembalikan Nilai Perkalian dengan menggunakan Parameter");
function kalikan(num1, num2) {
    return num1 * num2;
}
var num1 = 5;
var num2 = 5;

var hasilKali = kalikan(num1, num2);

console.log(hasilKali);
console.log("");

//Tugas No 3
console.log("Tugas No 3 Membuat Funtion Mengembalikan Gabungan String dengan mneggunakan Parameter");
function introduce(name, age, address, hobby) {
    return "Nama saya "+name+", umur saya "+age+" tahun, alamat saya di "+address+", dan saya punya hobby yaitu "+hobby+"!";
}

var name = "Sena"
var age = 22
var address = "Jln. Ijen, Malang"
var hobby = "Gaming"

var perkenalan = introduce(name, age, address, hobby);

console.log(perkenalan);
